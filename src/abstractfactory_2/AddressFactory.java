package abstractfactory_2;

public interface AddressFactory {
    public Address createAddress();
    public PhoneNumber createPhoneNumber();
}
