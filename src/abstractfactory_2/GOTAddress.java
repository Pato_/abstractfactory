/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory_2;

/**
 *
 * @author Jorge
 */
public class GOTAddress extends Address {
    private static final String COUNTRY = "GOTHAM";
    private static final String COMMA = ",";
    
    @Override
    public String getCountry() { return COUNTRY;}
    
    @Override
    public String getFullAddress() {
        return getStreet() + EOL_STRING + getPostalCode() + COMMA + SPACE + getCity() + SPACE +
                "(" + getRegion() + ")" + EOL_STRING + COUNTRY + EOL_STRING;
    }
    
}