/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory_2;

/**
 *
 * @author Jorge
 */
public class GOTAddressFactory implements AddressFactory {
    
    @Override
    public Address createAddress() {
        return new GOTAddress();
    } 
    
    @Override
    public PhoneNumber createPhoneNumber() {
        return new GOTPhoneNumber();
    }
}
